import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './UserRoomForm.module.css';
import send, {ErrorJSON} from "../../helpers/api";
import {ErrorContext} from "../../ErrorContext";
import {RouteComponentProps} from "react-router";
import Paging from "../../helpers/paging/Paging";
import {ProfileDTO} from "../../user/user/User";
import {userIcons} from "../../user/userFrame/UserFrame";
import {DormitoryDTO, RoomDTO} from "../../dormitory/dormitories/Dormitories";
import os1 from "../../assets/1os.svg";
import os2 from "../../assets/2os.svg";
import os3 from "../../assets/3os.svg";
import os0 from "../../assets/0os.svg";
import RoundedBox from "../../helpers/roundedBox/RoundedBox";

export interface RoomRentRequestDTO {
    sruUserUuid: string,
    roomUuid: string,
    dormitoryUuid: string,
}

export interface IProps extends RouteComponentProps {
    roomUuid?: string,
    userUuid?: string,
    dormitoryUuid?: string,
}

export interface IState {
    user?: ProfileDTO,
    users: ProfileDTO[],
    usersPage: number,
    usersPages: number,
    userNameSearch: string,
    userSurnameSearch: string,
    dormitory?: DormitoryDTO,
    dormitories: DormitoryDTO[],
    room?: RoomDTO,
    rooms: RoomDTO[],
    roomsPage: number,
    roomsPages: number,
    roomNameSearch: string,
}

export const roomPlaces: string[] = [os0, os1, os2, os3];

export default class UserRoomForm extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            users: [],
            usersPage: 0,
            usersPages: 0,
            userNameSearch: '',
            userSurnameSearch: '',
            dormitories: [],
            rooms: [],
            roomsPage: 0,
            roomsPages: 0,
            roomNameSearch: '',
        };
        this.handleChangeRoomName = this.handleChangeRoomName.bind(this);
        this.handleChangeUserName = this.handleChangeUserName.bind(this);
        this.handleChangeUserSurname = this.handleChangeUserSurname.bind(this);
        this.handleChangeDormitory = this.handleChangeDormitory.bind(this);
        this.handleChangeUsersPage = this.handleChangeUsersPage.bind(this);
        this.handleChangeRoomsPage = this.handleChangeRoomsPage.bind(this);
        this.handleSelectUser = this.handleSelectUser.bind(this);
        this.handleSelectRoom = this.handleSelectRoom.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeDormitory(event: ChangeEvent<HTMLSelectElement>) {
        this.setState({
            dormitory: this.findDormitoryByUuid(event.target.value),
            room: undefined,
        }, this.refreshRooms);
    }

    refreshRooms() {
        const {dormitory, roomNameSearch, roomsPage} = this.state;
        if (dormitory) {
            send('GET', `/dormitory/${dormitory.uuid}/room/search/`, {
                name: roomNameSearch,
                page: roomsPage,
            }, (result: { rooms: RoomDTO[], pages: number }) => {
                if (this.state.rooms !== result.rooms || this.state.roomsPages !== result.pages) {
                    this.setState({
                        rooms: result.rooms,
                        roomsPages: result.pages,
                        roomsPage: 0,
                    })
                }
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        } else {
            this.setState({
                rooms: [],
                room: undefined,
                roomsPages: 0,
                roomsPage: 0,
            });
        }
    }

    handleChangeRoomName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            roomNameSearch: event.target.value,
            roomsPage: 0,
        }, this.refreshRooms);
    }

    handleChangeUserName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            userNameSearch: event.target.value,
            usersPage: 0,
        }, this.refreshUsers);
    }

    handleChangeUserSurname(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            userSurnameSearch: event.target.value,
            usersPage: 0,
        }, this.refreshUsers);
    }

    handleChangeUsersPage(page: number) {
        this.setState({
            usersPage: page,
        }, this.refreshUsers);
    }

    handleChangeRoomsPage(page: number) {
        this.setState({
            roomsPage: page,
        }, this.refreshRooms);
    }

    handleSubmit(event: SyntheticEvent) {
        const {dormitory, room, user} = this.state;
        event.preventDefault();
        const {history} = this.props;
        if (dormitory && dormitory.uuid && room && room.uuid && user) {
            const body: RoomRentRequestDTO = {
                dormitoryUuid: dormitory.uuid,
                roomUuid: room.uuid,
                sruUserUuid: user.uuid,
            };

            send('POST', `/roomRent/`, body, () => {
                history.goBack();
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }

    componentDidMount() {
        this.refreshUsers();
        this.refreshDormitories();
        this.fetchDormitory();
        this.fetchRoom();
        this.fetchProfile();
    }

    refreshUsers() {
        const {userNameSearch, userSurnameSearch, usersPage} = this.state;

        send('GET', `/user/search/`, {
            name: userNameSearch,
            surname: userSurnameSearch,
            page: usersPage,
        }, (result: { users: ProfileDTO[], pages: number }) => {
            if (this.state.users !== result.users || this.state.usersPages !== result.pages) {
                this.setState({
                    users: result.users,
                    usersPages: result.pages
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    refreshDormitories() {
        send('GET', `/dormitory/`, {}, (result: DormitoryDTO[]) => {
            if (this.state.dormitories !== result) {
                this.setState({
                    dormitories: result,
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    findUserByUuid(uuid: string): ProfileDTO | undefined {
        const {users} = this.state;
        if (users) {
            const filtered = users.filter(user => user.uuid === uuid);
            if (filtered.length === 1) {
                return filtered[0];
            }
        }
        return undefined;
    }

    findRoomByUuid(uuid: string): RoomDTO | undefined {
        const {rooms} = this.state;
        if (rooms) {
            const filtered = rooms.filter(room => room.uuid === uuid);
            if (filtered.length === 1) {
                return filtered[0];
            }
        }
        return undefined;
    }

    findDormitoryByUuid(uuid: string): DormitoryDTO | undefined {
        const {dormitories} = this.state;
        if (dormitories) {
            const filtered = dormitories.filter(dormitory => dormitory.uuid === uuid);
            if (filtered.length === 1) {
                return filtered[0];
            }
        }
        return undefined;
    }

    handleSelectUser(event: ChangeEvent<HTMLInputElement>) {
        this.setState({user: this.findUserByUuid(event.target.value)});
    }

    handleSelectRoom(event: ChangeEvent<HTMLInputElement>) {
        this.setState({room: this.findRoomByUuid(event.target.value)});
    }

    fetchProfile() {
        const {userUuid} = this.props;
        if (userUuid) {
            send('GET', `/user/${userUuid}/`, {}, (result: ProfileDTO) => {
                if (this.state.user !== result) {
                    this.setState({
                        user: result,
                    })
                }
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        } else {
            this.setState({
                user: undefined,
            })
        }
    }

    fetchRoom() {
        const {dormitoryUuid, roomUuid} = this.props;
        if (dormitoryUuid && roomUuid) {
            send('GET', `/dormitory/${dormitoryUuid}/room/${roomUuid}/`, {}, (result: RoomDTO) => {
                if (this.state.room !== result) {
                    this.setState({
                        room: result,
                    })
                }
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        } else {
            this.setState({
                room: undefined,
            })
        }
    }

    fetchDormitory() {
        const {dormitoryUuid} = this.props;
        if (dormitoryUuid) {
            send('GET', `/dormitory/${dormitoryUuid}/`, {}, (result: DormitoryDTO) => {
                if (this.state.room !== result) {
                    this.setState({
                        dormitory: result,
                    })
                }
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        } else {
            this.setState({
                room: undefined,
            })
        }
    }

    render() {
        const {
            userNameSearch, userSurnameSearch, usersPage, usersPages, users,
            dormitories, rooms, roomsPage, roomsPages, roomNameSearch,
            user, room, dormitory
        } = this.state;
        const {roomUuid, userUuid} = this.props;

        if (!roomUuid && !userUuid) {
            return null;
        }

        return (
            <div className={styles.form}>
                <h3>Dodaj zameldowanie</h3>
                {!userUuid &&
                <RoundedBox>
                    <form>
                        <div>
                            <h4>Szukaj użytkownika</h4>
                            <input value={userNameSearch} onChange={this.handleChangeUserName} placeholder="Imię"/>
                            <input value={userSurnameSearch} onChange={this.handleChangeUserSurname}
                                   placeholder="Nazwisko"/>
                        </div>
                        <div>
                            <h4>Wybierz użytkownika</h4><br/>
                            {
                                users && users.map(user => (
                                    <div key={user.uuid}>
                                        <input required={true} type="radio" id={user.uuid} name="userUUID"
                                               value={user.uuid}
                                               checked={this.state.user?.uuid === user.uuid}
                                               onChange={this.handleSelectUser}/>
                                        <label htmlFor={user.uuid}>
                                            <img alt={user.userType} src={userIcons[user.userType]}/>
                                            {user.firstName} {user.lastName}
                                        </label><br/>
                                    </div>
                                ))
                            }
                            <Paging page={usersPage} pages={usersPages}
                                    onChange={this.handleChangeUsersPage}/>
                        </div>
                    </form>
                </RoundedBox>
                }
                {!roomUuid &&
                <RoundedBox>
                    <form>
                        <div>
                            <h4>Szukaj pokoju</h4>
                            {dormitories && dormitories.length > 0 &&
                            <select value={dormitory?.uuid} onChange={this.handleChangeDormitory}>
                                <option value=''>Wybierz DS</option>
                                {dormitories.map(dormitory =>
                                    <option value={dormitory.uuid} key={dormitory.uuid}>{dormitory.name}</option>
                                )}
                            </select>
                            }
                            {dormitory &&
                            <input value={roomNameSearch} onChange={this.handleChangeRoomName}
                                   placeholder="Nazwa pokoju"/>}
                        </div>
                        <div>
                            <h4>Wybierz pokój</h4><br/>
                            {
                                rooms && rooms.map(room => (
                                    <div key={room.uuid}>
                                        <input required={true} type="radio" id={room.uuid} name="roomUUID"
                                               checked={this.state.room?.uuid === room.uuid}
                                               value={room.uuid}
                                               onChange={this.handleSelectRoom}/>
                                        <label htmlFor={room.uuid}>
                                            <img alt={`liczba miejsc ${room.places}`} src={roomPlaces[room.places]}/>
                                            {room.name}
                                        </label><br/>
                                    </div>
                                ))
                            }
                            <Paging page={roomsPage} pages={roomsPages}
                                    onChange={this.handleChangeRoomsPage}/>
                        </div>
                    </form>
                </RoundedBox>
                }
                <h3>Podsumowanie</h3>
                <RoundedBox>
                    <form onSubmit={this.handleSubmit}>
                        <div>
                            <h4>Użytkownik</h4>
                            {user &&
                            <input value={`${user.firstName} ${user.lastName}`} disabled
                                   placeholder="Imię i nazwisko"/>}
                        </div>
                        <div>
                            <h4>Pokój</h4>
                            {room && dormitory &&
                            <input value={`${dormitory.name} ${room.name}`} disabled placeholder="Pokój"/>}
                        </div>
                        <button className={styles.button} type="submit">Dodaj zameldowanie</button>
                    </form>
                </RoundedBox>
            </div>
        );
    }
}

UserRoomForm.contextType = ErrorContext;
