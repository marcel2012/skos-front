import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import UserRoomForm from '../UserRoomForm';

interface Params {
    roomUuid?: string,
    userUuid?: string,
    dormitoryUuid?: string,
}

export interface IProps extends RouteComponentProps<Params> {
}


class UserRoomFormRouted extends React.Component<IProps, {}> {
    render() {
        const {roomUuid, userUuid, dormitoryUuid} = this.props.match.params;

        return <UserRoomForm dormitoryUuid={dormitoryUuid} roomUuid={roomUuid} userUuid={userUuid} {...this.props}/>;
    }
}


export default UserRoomFormRouted;
