import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './Devices.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {ErrorContext} from "../../ErrorContext";
import {DeviceResponseDTO} from "../deviceForm/DeviceForm";
import DeviceFrame from "../deviceFrame/DeviceFrame";
import Paging from "../../helpers/paging/Paging";
import RoundedBox from "../../helpers/roundedBox/RoundedBox";


export interface IProps extends WithTranslation {
}


export interface IState {
    hostName: string,
    macAddress: string,
    deviceType: string,
    devices: DeviceResponseDTO[],
    devicesPage: number,
    devicesPages: number,
}

class Devices extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            hostName: '',
            macAddress: '',
            deviceType: '',
            devices: [],
            devicesPage: 0,
            devicesPages: 0,
        };
        this.handleChangeHostName = this.handleChangeHostName.bind(this);
        this.handleChangeMacAddress = this.handleChangeMacAddress.bind(this);
        this.handleChangeDeviceType = this.handleChangeDeviceType.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.refreshDevices();
    }

    handleChangeHostName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            hostName: event.target.value,
            devicesPage: 0,
        }, this.refreshDevices);
    }

    handleChangeMacAddress(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            macAddress: event.target.value,
            devicesPage: 0,
        }, this.refreshDevices);
    }

    handleChangeDeviceType(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            deviceType: event.target.value,
            devicesPage: 0,
        }, this.refreshDevices);
    }

    handleChangePage(page: number) {
        this.setState({
            devicesPage: page,
        }, this.refreshDevices);
    }

    refreshDevices() {
        send('GET', `/device/search/`, {
            hostName: this.state.hostName,
            macAddress: this.state.macAddress,
            page: this.state.devicesPage
        }, (result: { devices: DeviceResponseDTO[], pages: number }) => {
            if (this.state.devices !== result.devices || this.state.devicesPages !== result.pages) {
                this.setState({
                    devices: result.devices,
                    devicesPages: result.pages,
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    handleSubmit(event: SyntheticEvent) {
        event.preventDefault();
        this.setState({
            devicesPage: 0,
        }, this.refreshDevices);
    }


    render() {
        const {hostName, macAddress, devices, devicesPage, devicesPages} = this.state;

        return (
            <div className={styles.form}>
                <RoundedBox>
                    <form onSubmit={this.handleSubmit}>
                        <h3>Szukaj urządzeń</h3>
                        <div>
                            <input value={hostName} onChange={this.handleChangeHostName} placeholder="Hostname"/>
                        </div>
                        <div>
                            <input value={macAddress} onChange={this.handleChangeMacAddress} placeholder="MAC address"/>
                        </div>
                        <button className={styles.button} type="submit">Szukaj</button>
                    </form>
                </RoundedBox>
                {devices && devices.length > 0 &&
                <div>
                    <h4>Urządzenia</h4>
                    {devices && devices.map(device => (
                        <DeviceFrame key={device.uuid} userUuid={device.userUuid} device={device}/>
                    ))}
                    <Paging page={devicesPage} pages={devicesPages} onChange={this.handleChangePage}/>
                </div>
                }
            </div>
        );
    }
}

export default withTranslation()(Devices);
Devices.contextType = ErrorContext;
