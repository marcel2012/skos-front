import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './DeviceForm.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import computer from '../../assets/computer.svg';
import router from '../../assets/router.svg';
import wifi from '../../assets/wifi.svg';
import send, {ErrorJSON} from "../../helpers/api";
import {withRouter} from "react-router-dom";
import {ErrorContext} from "../../ErrorContext";
import {RouteComponentProps} from "react-router";
import RoundedBox from "../../helpers/roundedBox/RoundedBox";

export interface IProps extends RouteComponentProps, WithTranslation {
    onAdded: Function,
    uuid?: string,
    userUuid?: string,
}

export interface IState {
    hostName: string,
    macAddress: string,
    deviceType: string,
}

export interface DeviceResponseDTO {
    uuid: string,
    userUuid?: string,
    hostName: string,
    macAddress: string,
    deviceType: string,
}

export interface DeviceRequestDTO {
    uuid: string,
    hostName: string,
    macAddress: string,
    deviceType: string,
}

const deviceTypes: { code: string, icon: string }[] = [
    {
        code: 'COMPUTER',
        icon: computer,
    },
    {
        code: 'ROUTER',
        icon: router,
    },
    {
        code: 'MOBILE_DEVICE',
        icon: wifi,
    },
    {
        code: 'SERVER',
        icon: wifi,
    },
    /*{
        name: 'OFFICE',
        icon: wifi,
    },*/
];

class DeviceForm extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            hostName: 'schabowy',
            macAddress: 'aa:bb:cc:dd:ee:ff',
            deviceType: deviceTypes[0].code,
        };
        this.handleChangeHostName = this.handleChangeHostName.bind(this);
        this.handleChangeMacAddress = this.handleChangeMacAddress.bind(this);
        this.handleChangeDeviceType = this.handleChangeDeviceType.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeHostName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({hostName: event.target.value});
    }

    handleChangeMacAddress(event: ChangeEvent<HTMLInputElement>) {
        this.setState({macAddress: event.target.value});
    }

    handleChangeDeviceType(event: ChangeEvent<HTMLInputElement>) {
        this.setState({deviceType: event.target.value});
    }

    handleSubmit(event: SyntheticEvent) {
        event.preventDefault();
        const {history, userUuid} = this.props;

        const url = `/user/${userUuid || 'me'}/device/`;

        send('PUT', url, {
            uuid: this.props.uuid,
            hostName: this.state.hostName,
            macAddress: this.state.macAddress,
            deviceType: this.state.deviceType,
            active: true,
        }, () => {
            if (this.props.onAdded) {
                this.props.onAdded();
            } else {
                history.goBack();
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    componentDidMount() {
        this.getDeviceInfo();
    }

    getDeviceInfo() {
        const {uuid, userUuid} = this.props;
        if (uuid) {
            const url = `/user/${userUuid || 'me'}/device/${uuid}/`;
            send('GET', url, {}, (result: DeviceResponseDTO) => {
                this.setState({
                    hostName: result.hostName,
                    macAddress: result.macAddress,
                    deviceType: result.deviceType,
                })
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }


    render() {
        const {t} = this.props;
        const {hostName, macAddress, deviceType} = this.state;
        const {uuid} = this.props;

        return (
            <div className={styles.form}>
                {uuid && <h3>Edytuj urządzenie</h3>}
                {!uuid && <h3>Dodaj urządzenie</h3>}
                <RoundedBox>
                    <form onSubmit={this.handleSubmit}>
                        <div>
                            <h4>Dane podstawowe</h4>
                            <input value={hostName} onChange={this.handleChangeHostName} placeholder="Nazwa urządzenia"
                                   pattern="[A-Za-z0-9]*" minLength={6} maxLength={20} required={true}/>
                            {hostName.length > 0 && (
                                <span>
                                    <h4>Dodatkowe dane</h4>
                                    <input value={macAddress} onChange={this.handleChangeMacAddress}
                                           placeholder="Adres MAC"
                                           minLength={6} maxLength={20} required={true}/>
                                </span>
                            )}
                        </div>
                        <div>
                            <h4>Wybierz typ urządzenia</h4><br/>
                            {
                                deviceTypes && deviceTypes.map(type => (
                                    <span key={type.code}>
                                        <input type="radio" id={type.code} name="deviceType" value={type.code}
                                               checked={deviceType === type.code}
                                               onChange={this.handleChangeDeviceType}/>
                                        <label htmlFor={type.code}>
                                            <img alt={t(type.code)} src={type.icon}/>
                                            {t(type.code)}
                                        </label><br/>
                                    </span>
                                ))
                            }
                        </div>
                        <button className={styles.button} type="submit">Zapisz</button>
                    </form>
                </RoundedBox>
            </div>
        );
    }
}

export default withTranslation()(withRouter(DeviceForm));
DeviceForm.contextType = ErrorContext;
