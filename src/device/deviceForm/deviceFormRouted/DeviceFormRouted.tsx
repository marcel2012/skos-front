import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import DeviceForm from '../DeviceForm';

interface Params {
    uuid?: string,
    userUuid?: string,
}

export interface IProps extends RouteComponentProps<Params> {
    hideBackButton: boolean,
    onAdded: Function,
}


class DeviceFormRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid, userUuid} = this.props.match.params;

        return <DeviceForm uuid={uuid} userUuid={userUuid} {...this.props}/>;
    }
}


export default DeviceFormRouted;
