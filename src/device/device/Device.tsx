import React from 'react';
import styles from './Device.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import computer from '../../assets/computer.svg';
import router from '../../assets/router.svg';
import ok from '../../assets/ok.svg';
import wait from '../../assets/wait.svg';
import ethernet from '../../assets/ethernet.svg';
import send, {ErrorJSON} from "../../helpers/api";
import {Link, withRouter} from "react-router-dom";
import {DeviceRequestDTO, DeviceResponseDTO} from "../deviceForm/DeviceForm";
import wifi from "../../assets/wifi.svg";
import {ErrorContext} from "../../ErrorContext";
import {RouteComponentProps} from "react-router";
import {ProfileDTO} from "../../user/user/User";
import {formatDateString} from "../../helpers/dateFormat";
import {DHCPLeaseResponseDTO} from "../deviceFrame/DeviceFrame";
import UserFrame from "../../user/userFrame/UserFrame";
import RollingBox from '../../helpers/rollingBox/RollingBox';
import RoundedBox from '../../helpers/roundedBox/RoundedBox';
import TableBox from "../../helpers/tableBox/TableBox";

export interface IProps extends RouteComponentProps, WithTranslation {
    uuid?: string,
    userUuid?: string,
}

interface DeviceHistoryDTO extends DeviceResponseDTO {
    validFrom: string,
    modifiedBy: ProfileDTO,
}

export interface IState {
    device?: DeviceRequestDTO,
    deviceActive?: boolean,
    historyList?: DeviceHistoryDTO[],
    activeDHCPList?: DHCPLeaseResponseDTO[],
    dhcpList?: DHCPLeaseResponseDTO[],
    user?: ProfileDTO,
}

const deviceIcons: { [id: string]: string; } = {
    'COMPUTER': computer,
    'ROUTER': router,
    'MOBILE_DEVICE': wifi,
    'SERVER': wifi,
    'OFFICE': wifi,
};

class Device extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {};
        this.deactivateDevice = this.deactivateDevice.bind(this);
        this.activateDevice = this.activateDevice.bind(this);
    }

    activateDevice() {
        this.setDeviceActive(true);
    }

    deactivateDevice() {
        this.setDeviceActive(false);
    }

    componentDidMount() {
        this.refreshDevice();
        this.refreshDeviceHistory();
        this.refreshDeviceDHCP();
        this.refreshDeviceInactiveDHCP();
        this.refreshProfile();
    }

    refreshDevice() {
        const {uuid, userUuid} = this.props;
        if (uuid) {
            const url = `/user/${userUuid || 'me'}/device/${uuid}/`;
            send('GET', url, {}, (result: DeviceResponseDTO) => {
                this.setState({
                    device: result,
                })
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }

    refreshProfile() {
        const {userUuid} = this.props;
        if (userUuid) {
            const url = `/user/${userUuid}/`;
            send('GET', url, {}, (result: ProfileDTO) => {
                if (this.state.user !== result) {
                    this.setState({
                        user: result,
                    })
                }
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }

    refreshDeviceDHCP() {
        const {uuid, userUuid} = this.props;
        if (uuid) {
            const url = `/user/${userUuid || 'me'}/device/${uuid}/dhcp/`;
            send('GET', url, {}, (result: DHCPLeaseResponseDTO[]) => {
                const deviceActive: boolean = result.length > 0;
                if (this.state.deviceActive !== deviceActive || this.state.activeDHCPList !== result) {
                    this.setState({
                        deviceActive: deviceActive,
                        activeDHCPList: result,
                    });
                }
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }

    refreshDeviceInactiveDHCP() {
        const {uuid, userUuid} = this.props;
        if (uuid && userUuid) {
            const url = `/user/${userUuid}/device/${uuid}/dhcp/inactive/`;
            send('GET', url, {}, (result: DHCPLeaseResponseDTO[]) => {
                if (this.state.dhcpList !== result) {
                    this.setState({
                        dhcpList: result,
                    });
                }
            }, () => {
            });
        }
    }

    refreshDeviceHistory() {
        const {uuid, userUuid} = this.props;
        if (uuid && userUuid) {
            send('GET', `/device/${uuid}/history/`, {}, (result: DeviceHistoryDTO[]) => {
                this.setState({
                    historyList: result,
                })
            }, () => {
            });
        }
    }

    setDeviceActive(active: boolean) {
        const {uuid, userUuid} = this.props;
        const url = `/user/${userUuid || 'me'}/device/${uuid}/dhcp/`;
        send(active ? 'POST' : 'DELETE', url, {}, () => {
            this.refreshDeviceDHCP();
            this.refreshDeviceInactiveDHCP();
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    deactivateLease(uuid: string) {
        send('DELETE', `/dhcp/${uuid}/`, {}, () => {
            this.refreshDeviceDHCP();
            this.refreshDeviceInactiveDHCP();
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    render() {
        //const {t} = this.props;
        const {
            device, historyList, deviceActive, activeDHCPList, dhcpList, user
        } = this.state;
        const {userUuid} = this.props;

        if (!device) {
            return null;
        }

        const {uuid, hostName, macAddress, deviceType} = device;

        return (
            <div>
                <RoundedBox>
                    <div className={`${styles.deviceFrame} ${deviceActive ? '' : styles.inactive}`}>
                        <h4><img alt={"typ urządzenia"}
                                 src={deviceIcons[deviceType]}/>{hostName} ({macAddress})</h4>
                        {deviceActive &&
                        <span>
                                <p><img alt={"podłączone"} src={ok}/>Urządzenie podłączone do sieci</p>
                                <p><img alt={"prędkość"} src={ethernet}/>Prędkość 100 Mb/s</p>
                                <p><img alt={"konfiguracja DHCP"} src={wait}/>Urządzenie nie pobrało konfiguracji z serwera DHCP</p>
                            </span>
                        }
                        <Link to={`${userUuid ? `/user/${userUuid}` : ''}/device/${uuid}/edit/`}
                              className={styles.link}>Edytuj</Link>
                        {deviceActive &&
                        <button className={styles.button} onClick={this.deactivateDevice}
                                type="submit">Dezaktywuj</button>
                        }
                        {!deviceActive &&
                        <button className={styles.button} onClick={this.activateDevice}
                                type="submit">Aktywuj</button>}
                    </div>
                </RoundedBox>
                {userUuid && user &&
                <RollingBox title="Właściciel" defaultOpened={true}>
                    <UserFrame user={user}/>
                </RollingBox>
                }
                {userUuid && activeDHCPList &&
                <RollingBox title="Adresy IP" defaultOpened={true}>
                    <TableBox>
                        <table className={styles.table}>
                            <tbody>
                            <tr>
                                <th>Data od</th>
                                <th>Adres IP</th>
                                <th>Akcje</th>
                            </tr>
                            {activeDHCPList.map((dhcpElement) => (
                                <tr key={dhcpElement.uuid}>
                                    <td>{formatDateString(dhcpElement.validFrom)}</td>
                                    <td>{dhcpElement.ipAddress}</td>
                                    <td>
                                        <button onClick={() => this.deactivateLease(dhcpElement.uuid)}
                                                className={styles.link}>Zakończ
                                        </button>
                                    </td>
                                </tr>
                            ))
                            }
                            </tbody>
                        </table>
                    </TableBox>
                    <Link to={`/user/${userUuid}/device/${uuid}/ip/add/`} className={styles.link}>Dodaj</Link>
                </RollingBox>
                }
                {dhcpList && dhcpList.length > 0 &&
                <RollingBox title="Historyczne dzierżawy IP">
                    <TableBox>
                        <table className={styles.table}>
                            <tbody>
                            <tr>
                                <th>Data od</th>
                                <th>Data do</th>
                                <th>Adres IP</th>
                            </tr>
                            {dhcpList.map((dhcpElement) => (
                                <tr key={dhcpElement.uuid}>
                                    <td>{formatDateString(dhcpElement.validFrom)}</td>
                                    <td>{formatDateString(dhcpElement.validTo)}</td>
                                    <td>{dhcpElement.ipAddress}</td>
                                </tr>
                            ))
                            }
                            </tbody>
                        </table>
                    </TableBox>
                </RollingBox>
                }
                {historyList && historyList.length > 0 &&
                <RollingBox title="Historia">
                    <TableBox>
                        <table className={styles.table}>
                            <tbody>
                            <tr>
                                <th>Data zmiany</th>
                                <th>Nazwa</th>
                                <th>Typ</th>
                                <th>MAC</th>
                                <th>Edytowane przez</th>
                            </tr>
                            {historyList.map((historyElement) => (
                                <tr key={historyElement.uuid}>
                                    <td>{formatDateString(historyElement.validFrom)}</td>
                                    <td>{historyElement.hostName}</td>
                                    <td>
                                        <img className={styles.deviceTypeIcon} alt={"komputer"}
                                             src={deviceIcons[historyElement.deviceType]}/> {historyElement.deviceType}
                                    </td>
                                    <td>{historyElement.macAddress}</td>
                                    <td>
                                        <Link to={`/user/${historyElement.modifiedBy.uuid}/`}>
                                            {historyElement.modifiedBy.firstName} {historyElement.modifiedBy.lastName}
                                        </Link>
                                    </td>
                                </tr>
                            ))
                            }
                            </tbody>
                        </table>
                    </TableBox>
                </RollingBox>
                }
                <RollingBox title="Firewall">
                </RollingBox>
            </div>

        );
    }
}

export default withTranslation()(withRouter(Device));
Device.contextType = ErrorContext;
