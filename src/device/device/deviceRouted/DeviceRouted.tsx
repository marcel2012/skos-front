import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import Device from "../Device";

interface Params {
    uuid?: string
    userUuid?: string
}

export interface IProps extends RouteComponentProps<Params> {
}


class DeviceRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid, userUuid} = this.props.match.params;

        return <Device uuid={uuid} userUuid={userUuid}/>;
    }
}


export default DeviceRouted;
