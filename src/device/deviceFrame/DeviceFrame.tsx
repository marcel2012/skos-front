import React from 'react';
import styles from './DeviceFrame.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import computer from '../../assets/computer.svg';
import router from '../../assets/router.svg';
import send, {ErrorJSON} from "../../helpers/api";
import {Link} from "react-router-dom";
import {DeviceResponseDTO} from "../deviceForm/DeviceForm";
import wifi from "../../assets/wifi.svg";
import {ErrorContext} from "../../ErrorContext";
import RoundedBox from '../../helpers/roundedBox/RoundedBox';

export interface IProps extends WithTranslation {
    device: DeviceResponseDTO,
    userUuid?: string,
}

export interface IState {
    device: DeviceResponseDTO,
    deviceActive?: boolean,
}

export interface DHCPLeaseResponseDTO {
    uuid: string,
    ipAddress: string,
    validFrom: string,
    validTo: string,
}

const deviceIcons: { [id: string]: string; } = {
    'COMPUTER': computer,
    'ROUTER': router,
    'MOBILE_DEVICE': wifi,
    'SERVER': wifi,
    'OFFICE': wifi,
};

class DeviceFrame extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        const {device} = this.props;
        this.state = {
            device: device,
        };
    }

    componentDidMount() {
        this.refreshDeviceDHCP();
    }

    refreshDeviceDHCP() {
        const {device} = this.state;
        const {userUuid} = this.props;
        if (!device) {
            return;
        }
        const {uuid} = device;
        if (uuid) {
            const url = `/user/${userUuid || 'me'}/device/${uuid}/dhcp/`;
            send('GET', url, {}, (result: DHCPLeaseResponseDTO[]) => {
                const deviceActive: boolean = result.length > 0;
                if (this.state.deviceActive !== deviceActive) {
                    this.setState({
                        deviceActive: deviceActive,
                    });
                }
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }

    render() {
        const {device, deviceActive} = this.state;
        const {userUuid} = this.props;

        if (!device) {
            return null;
        }

        const {uuid, hostName, macAddress, deviceType} = device;

        return (
            <RoundedBox>
                <div className={`${styles.deviceFrame} ${deviceActive ? '' : styles.inactive}`}>
                    <h4><img alt={"komputer"}
                             src={deviceIcons[deviceType]}/>{hostName} ({macAddress})</h4>
                    <Link to={`${userUuid ? `/user/${userUuid}` : ''}/device/${uuid}/`}
                          className={styles.link}>Sczegóły</Link>
                </div>
            </RoundedBox>
        );
    }
}

export default withTranslation()(DeviceFrame);
DeviceFrame.contextType = ErrorContext;
