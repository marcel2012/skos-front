import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './Users.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {Link} from "react-router-dom";
import {ErrorContext} from "../../ErrorContext";
import {ProfileDTO} from '../user/User';
import Paging from "../../helpers/paging/Paging";
import UserFrame from '../userFrame/UserFrame';
import RoundedBox from "../../helpers/roundedBox/RoundedBox";


export interface IProps extends WithTranslation {
}


export interface IState {
    name: string,
    surname: string,
    email: string,
    login: string,
    users: ProfileDTO[],
    usersPage: number,
    usersPages: number,
}

class Users extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            name: '',
            surname: '',
            email: '',
            login: '',
            users: [],
            usersPage: 0,
            usersPages: 0,
        };
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeSurname = this.handleChangeSurname.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.refreshUsers();
    }

    handleChangeName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            name: event.target.value,
            usersPage: 0,
        }, this.refreshUsers);
    }

    handleChangeSurname(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            surname: event.target.value,
            usersPage: 0,
        }, this.refreshUsers);
    }

    handleChangeEmail(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            email: event.target.value,
            usersPage: 0,
        }, this.refreshUsers);
    }

    handleChangeLogin(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            login: event.target.value,
            usersPage: 0,
        }, this.refreshUsers);
    }

    handleChangePage(page: number) {
        this.setState({
            usersPage: page,
        }, this.refreshUsers);
    }

    refreshUsers() {
        send('GET', `/user/search/`, {
            name: this.state.name,
            surname: this.state.surname,
            email: this.state.email,
            login: this.state.login,
            page: this.state.usersPage,
        }, (result: { users: ProfileDTO[], pages: number }) => {
            if (this.state.users !== result.users || this.state.usersPages !== result.pages) {
                this.setState({
                    users: result.users,
                    usersPages: result.pages
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    handleSubmit(event: SyntheticEvent) {
        event.preventDefault();
        this.setState({
            usersPage: 0,
        }, this.refreshUsers);
    }


    render() {
        const {name, surname, login, email, users, usersPage, usersPages} = this.state;

        return (
            <div className={styles.form}>
                <RoundedBox>
                    <form onSubmit={this.handleSubmit}>
                        <h3>Szukaj użytkownika</h3>
                        <div>
                            <input value={name} onChange={this.handleChangeName} placeholder="Imię"/>
                        </div>
                        <div>
                            <input value={surname} onChange={this.handleChangeSurname} placeholder="Nazwisko"/>
                        </div>
                        <div>
                            <input value={login} onChange={this.handleChangeLogin} placeholder="Login"/>
                        </div>
                        <div>
                            <input value={email} type="email" onChange={this.handleChangeEmail} placeholder="Email"/>
                        </div>
                        <button className={styles.button} type="submit">Szukaj</button>
                    </form>
                </RoundedBox>
                {users && users.length > 0 &&
                <div>
                    <h4>Użytkownicy</h4>
                    {users.map(user => <UserFrame key={user.uuid} user={user}/>)}
                    <Paging page={usersPage} pages={usersPages}
                            onChange={(page: number) => this.handleChangePage(page)}/>
                </div>
                }
                <Link to="/user/add/" className={styles.link}>Utwórz nowe konto</Link>
            </div>
        );
    }
}

export default withTranslation()(Users);
Users.contextType = ErrorContext;
