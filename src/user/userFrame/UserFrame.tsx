import React from 'react';
import styles from './UserFrame.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import tourist from "../../assets/tourist.svg";
import student from "../../assets/student.svg";
import administrator from "../../assets/administrator.svg";
import office from "../../assets/office.svg";
import reception from "../../assets/reception.svg";
import {ErrorContext} from "../../ErrorContext";
import {ProfileDTO, RoomRentResponseDTO} from "../user/User";
import send, {ErrorJSON} from "../../helpers/api";
import RoundedBox from "../../helpers/roundedBox/RoundedBox";

export interface IProps extends WithTranslation {
    user: ProfileDTO,
}

export interface IState {
    activeRoomRents?: RoomRentResponseDTO[],
}

export const userIcons: { [id: string]: string; } = {
    'DEFAULT': tourist,
    'STUDENT': student,
    'ADMINISTRATOR': administrator,
    'DORMITORY_OFFICE': office,
    'RECEPTION': reception,
};

class UserFrame extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.refreshActiveRoomRents();
    }

    refreshActiveRoomRents() {
        const {user} = this.props;
        const url = `/user/${user.uuid}/roomRent/`;
        send('GET', url, {}, (result: RoomRentResponseDTO[]) => {
            if (this.state.activeRoomRents !== result) {
                this.setState({
                    activeRoomRents: result,
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    render() {
        const {user} = this.props;
        const {activeRoomRents} = this.state;

        if (!user || !activeRoomRents) {
            return null;
        }

        const {uuid, firstName, lastName, userType} = user;

        return (
            <RoundedBox>
                <div className={styles.userFrame}>
                    <h4>
                        <img alt={"komputer"} src={userIcons[userType]}/>
                        {firstName} {lastName}
                    </h4>
                    {activeRoomRents && activeRoomRents.length > 0 && activeRoomRents.map(rent =>
                        <span>
                            <p>
                                <img alt={"akademik"}
                                     src={reception}/>
                                <span>
                                    <Link to={`/dormitory/${rent.dormitory.uuid}/`}>
                                        {rent.dormitory.name}
                                    </Link>
                                    <span> </span>
                                    <Link to={`/dormitory/${rent.dormitory.uuid}/room/${rent.room.uuid}/`}>
                                        {rent.room.name}
                                    </Link>
                                </span>
                            </p>
                        </span>
                    )}
                    <Link to={`/user/${uuid}/`} className={styles.link}>Sczegóły</Link>
                </div>
            </RoundedBox>
        );
    }
}

export default withTranslation()(UserFrame);
UserFrame.contextType = ErrorContext;
