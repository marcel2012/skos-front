import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import UserForm from '../UserForm';

interface Params {
    uuid?: string
}

export interface IProps extends RouteComponentProps<Params> {
    hideBackButton: boolean,
    onAdded: Function,
}


class UserFormRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid} = this.props.match.params;

        return <UserForm uuid={uuid} {...this.props}/>;
    }
}


export default UserFormRouted;
