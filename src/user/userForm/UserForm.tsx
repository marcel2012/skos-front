import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './UserForm.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {withRouter} from "react-router-dom";
import {ErrorContext} from "../../ErrorContext";
import {RouteComponentProps} from "react-router";
import {ProfileDTO} from "../user/User";
import {userIcons} from '../userFrame/UserFrame';
import RoundedBox from "../../helpers/roundedBox/RoundedBox";

export interface IProps extends RouteComponentProps, WithTranslation {
    onAdded: Function,
    uuid?: string,
}

export interface IState {
    firstName: string,
    lastName: string,
    userType: string,
}

class UserForm extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            userType: 'STUDENT',
        };
        this.handleChangeFirstName = this.handleChangeFirstName.bind(this);
        this.handleChangeLastName = this.handleChangeLastName.bind(this);
        this.handleChangeUserType = this.handleChangeUserType.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeFirstName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({firstName: event.target.value});
    }

    handleChangeLastName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({lastName: event.target.value});
    }

    handleChangeUserType(event: ChangeEvent<HTMLInputElement>) {
        this.setState({userType: event.target.value});
    }

    handleSubmit(event: SyntheticEvent) {
        event.preventDefault();
        const {history} = this.props;

        send('PUT', '/user', {
            uuid: this.props.uuid,
            name: this.state.firstName,
            surname: this.state.lastName,
            userType: this.state.userType,
        }, () => {
            if (this.props.onAdded) {
                this.props.onAdded();
            } else {
                history.goBack();
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    componentDidMount() {
        const {uuid} = this.props;
        if (uuid) {
            send('GET', `/user/${uuid}/`, {}, (result: ProfileDTO) => {
                this.setState({
                    firstName: result.firstName,
                    lastName: result.lastName,
                    userType: result.userType,
                })
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }


    render() {
        const {t} = this.props;
        const {firstName, lastName, userType} = this.state;
        const {uuid} = this.props;

        return (
            <div className={styles.form}>
                {uuid && <h3>Edytuj użytkownika</h3>}
                {!uuid && <h3>Dodaj użytkownika</h3>}
                <RoundedBox>
                    <form onSubmit={this.handleSubmit}>
                        <div>
                            <h4>Dane podstawowe</h4>
                            <input value={firstName} onChange={this.handleChangeFirstName} placeholder="Imię"
                                   required={true}/>
                            <input value={lastName} onChange={this.handleChangeLastName} placeholder="Nazwisko"
                                   required={true}/>
                        </div>
                        <div>
                            <h4>Wybierz typ konta</h4><br/>
                            {
                                Object.keys(userIcons).map(key => (
                                    <span key={key}>
                                    <input type="radio" id={`usertype-${key}`} name="userType" value={key}
                                           checked={userType === key} onChange={this.handleChangeUserType}/>
                                    <label htmlFor={`usertype-${key}`}>
                                        <img alt={t(key)} src={userIcons[key]}/>
                                        {t(key)}
                                    </label>
                                    <br/>
                                </span>
                                ))
                            }
                        </div>
                        <button className={styles.button} type="submit">Zapisz</button>
                    </form>
                </RoundedBox>
            </div>
        );
    }
}

export default withTranslation()(withRouter(UserForm));
UserForm.contextType = ErrorContext;
