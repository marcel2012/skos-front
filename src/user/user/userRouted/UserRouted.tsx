import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import Profile from '../User';

interface Params {
    uuid?: string
}

export interface IProps extends RouteComponentProps<Params> {
}


class UserRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid} = this.props.match.params;

        return <Profile uuid={uuid} {...this.props}/>;
    }
}

export default UserRouted;
