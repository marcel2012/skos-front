import styles from './User.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {Link} from "react-router-dom";
import DeviceForm, {DeviceResponseDTO} from "../../device/deviceForm/DeviceForm";
import DeviceFrame from "../../device/deviceFrame/DeviceFrame";
import {ErrorContext} from "../../ErrorContext";
import React from 'react';
import reception from "../../assets/reception.svg";
import email from "../../assets/email.svg";
import {DormitoryDTO, RoomDTO} from "../../dormitory/dormitories/Dormitories";
import {formatDateString} from "../../helpers/dateFormat";
import {userIcons} from "../userFrame/UserFrame";
import RollingBox from "../../helpers/rollingBox/RollingBox";
import TableBox from "../../helpers/tableBox/TableBox";

export interface IProps extends WithTranslation {
    uuid?: string,
}

export interface ProfileDTO {
    uuid: string,
    firstName: string,
    lastName: string,
    room: RoomDTO,
    email: string,
    studentId: number,
    userType: string,
}

export interface IState {
    devices: DeviceResponseDTO[],
    user?: ProfileDTO,
    historyList?: SruUserHistoryDTO[],
    activeRoomRents?: RoomRentResponseDTO[],
    inactiveRoomRents?: RoomRentResponseDTO[],
}

interface SruUserHistoryDTO extends ProfileDTO {
    validFrom: string,
    modifiedBy: ProfileDTO,
}

export interface RoomRentResponseDTO {
    uuid: string,
    validFrom: string,
    validTo: string,
    user: ProfileDTO,
    room: RoomDTO,
    dormitory: DormitoryDTO,
}

class User extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            devices: [],
        };
    }

    componentDidMount() {
        this.refreshDevices();
        this.refreshProfile();
        this.refreshProfileHistory();
        this.refreshActiveRoomRents();
        this.refreshInactiveRoomRents();
    }

    refreshDevices() {
        const {uuid} = this.props;
        const url = `/user/${uuid || 'me'}/device/`;
        send('GET', url, {}, (result: DeviceResponseDTO[]) => {
            if (this.state.devices !== result) {
                this.setState({
                    devices: result,
                });
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    refreshProfile() {
        const {uuid} = this.props;
        const url = `/user/${uuid || 'me'}/`;
        send('GET', url, {}, (result: ProfileDTO) => {
            if (this.state.user !== result) {
                this.setState({
                    user: result,
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    refreshActiveRoomRents() {
        const {uuid} = this.props;
        const url = `/user/${uuid || 'me'}/roomRent/`;
        send('GET', url, {}, (result: RoomRentResponseDTO[]) => {
            if (this.state.activeRoomRents !== result) {
                this.setState({
                    activeRoomRents: result,
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    refreshInactiveRoomRents() {
        const {uuid} = this.props;
        const url = `/user/${uuid || 'me'}/roomRent/inactive/`;
        send('GET', url, {}, (result: RoomRentResponseDTO[]) => {
            if (this.state.activeRoomRents !== result) {
                this.setState({
                    inactiveRoomRents: result,
                })
            }
        }, () => {
        });
    }

    refreshProfileHistory() {
        const {uuid} = this.props;
        if (uuid) {
            send('GET', `/user/${uuid}/history/`, {}, (result: SruUserHistoryDTO[]) => {
                this.setState({
                    historyList: result,
                })
            }, () => {
            });
        }
    }

    deactivateRent(uuid: string) {
        send('DELETE', `/roomRent/${uuid}/`, {}, () => {
            this.refreshActiveRoomRents();
            this.refreshInactiveRoomRents();
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    render() {
        const {
            devices, user, historyList, activeRoomRents, inactiveRoomRents,
        } = this.state;
        const {uuid} = this.props;

        if (user == null || devices == null) {
            return null;
        }

        return (
            <div>
                <h3>{user.firstName} {user.lastName} {user.studentId &&
                <span className={styles.studentId}>#{user.studentId}</span>}</h3>
                {user.email && <p><img className={styles.icon} alt={"email"}
                                       src={email}/>
                    {uuid &&
                    <a href={`mailto:${user.email}`}>
                        {user.email}
                    </a>
                    }
                    {!uuid && user.email}
                </p>}
                {activeRoomRents && activeRoomRents.length > 0 && activeRoomRents.map(rent =>
                    <p>
                        <img className={styles.icon} alt={"akademik"} src={reception}/>
                        {uuid &&
                        <span>
                            <Link to={`/dormitory/${rent.dormitory.uuid}/`}>
                                {rent.dormitory.name}
                            </Link>
                            <span> </span>
                            <Link to={`/dormitory/${rent.dormitory.uuid}/room/${rent.room.uuid}/`}>
                                {rent.room.name}
                            </Link>
                        </span>
                        }
                        {!uuid && `${rent.dormitory.name} ${rent.room.name}`}
                    </p>
                )}
                {uuid &&
                <Link to={`/user/${uuid}/edit/`} className={styles.link}>Edytuj</Link>
                }
                {activeRoomRents && activeRoomRents.length === 0 && uuid &&
                <Link to={`/user/${uuid}/room/lease/add/`} className={styles.link}>Zamelduj</Link>
                }
                {devices && devices.length > 0 &&
                <div>
                    <h4>Urządzenia</h4>
                    {devices && devices.map(device => (
                        <DeviceFrame userUuid={uuid} key={device.uuid} device={device}/>
                    ))
                    }
                </div>
                }
                {!uuid && devices && devices.length === 0 &&
                <DeviceForm onAdded={() => this.refreshDevices()}/>
                }
                {!uuid && devices && devices.length > 0 &&
                <Link to="/device/add/" className={styles.link}>Dodaj nowe urządzenie</Link>
                }
                {uuid &&
                <Link to={`/user/${uuid}/device/add/`} className={styles.link}>Dodaj nowe
                    urządzenie</Link>
                }
                {uuid && activeRoomRents && activeRoomRents.length > 0 &&
                <RollingBox title='Zakwaterowanie'>
                    <TableBox>
                        <table className={styles.table}>
                            <tbody>
                            <tr>
                                <th>Data od</th>
                                <th>Pokój</th>
                                <th>Akcje</th>
                            </tr>
                            {activeRoomRents.map((historyElement) => (
                                <tr key={historyElement.uuid}>
                                    <td>{formatDateString(historyElement.validFrom)}</td>
                                    <td>
                                        <Link to={`/dormitory/${historyElement.dormitory.uuid}/`}>
                                            {historyElement.dormitory.name}
                                        </Link>
                                        <span> </span>
                                        <Link
                                            to={`/dormitory/${historyElement.dormitory.uuid}/room/${historyElement.room.uuid}/`}>
                                            {historyElement.room.name}
                                        </Link>
                                    </td>
                                    <td>
                                        <button onClick={() => this.deactivateRent(historyElement.uuid)}
                                                className={styles.link}>
                                            Zakończ
                                        </button>
                                    </td>
                                </tr>
                            ))
                            }
                            </tbody>
                        </table>
                    </TableBox>
                    <Link to={`/user/${uuid}/room/lease/add/`} className={styles.link}>Zamelduj</Link>
                </RollingBox>
                }
                {inactiveRoomRents && inactiveRoomRents.length > 0 &&
                <RollingBox title='Historia zakwaterowań'>
                    <TableBox>
                        <table className={styles.table}>
                            <tbody>
                            <tr>
                                <th>Data od</th>
                                <th>Data do</th>
                                <th>Pokój</th>
                            </tr>
                            {inactiveRoomRents.map((historyElement) => (
                                <tr key={historyElement.uuid}>
                                    <td>{formatDateString(historyElement.validFrom)}</td>
                                    <td>{formatDateString(historyElement.validTo)}</td>
                                    <td>
                                        <Link to={`/dormitory/${historyElement.dormitory.uuid}/`}>
                                            {historyElement.dormitory.name}
                                        </Link>
                                        <span> </span>
                                        <Link
                                            to={`/dormitory/${historyElement.dormitory.uuid}/room/${historyElement.room.uuid}/`}>
                                            {historyElement.room.name}
                                        </Link>
                                    </td>
                                </tr>
                            ))
                            }
                            </tbody>
                        </table>
                    </TableBox>
                </RollingBox>
                }
                {historyList && historyList.length > 0 &&
                <RollingBox title='Historia profilu'>
                    <TableBox>
                        <table className={styles.table}>
                            <tbody>
                            <tr>
                                <th>Data zmiany</th>
                                <th>Imię i nazwisko</th>
                                <th>Email</th>
                                <th>Numer albumu</th>
                                <th>Typ konta</th>
                                <th>Zmienione przez</th>
                            </tr>
                            {historyList.map((historyElement) => (
                                <tr key={historyElement.uuid}>
                                    <td>{formatDateString(historyElement.validFrom)}</td>
                                    <td>{historyElement.firstName} {historyElement.lastName}</td>
                                    <td>{historyElement.email}</td>
                                    <td>{historyElement.studentId}</td>
                                    <td>
                                        <img alt={"komputer"}
                                             src={userIcons[historyElement.userType]}/>
                                        {historyElement.userType}</td>
                                    <td>
                                        <Link to={`/user/${historyElement.modifiedBy.uuid}/`}>
                                            {historyElement.modifiedBy.firstName} {historyElement.modifiedBy.lastName}
                                        </Link>
                                    </td>
                                </tr>
                            ))
                            }
                            </tbody>
                        </table>
                    </TableBox>
                </RollingBox>
                }
            </div>
        );
    }
}

export default withTranslation()(User);
User.contextType = ErrorContext;
