import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

i18n.use(LanguageDetector).init({
    // we init with resources
    resources: {
        en: {
            translations: {
                sru: 'SRU v5',
                terms: 'Terms',
                contact: 'Contact',
                login: 'Log in',
                contact_email: 'Email',
                duty_hours: 'Duty hours',
                sru_login: 'Log in to SRU',
                login_placeholder: 'Login or email',
                password_placeholder: 'Password',
                login_submit: 'Log in',
                forgot_link: 'Forgot password?',
                forgot_password: 'Recover password',
                or: 'or',
                forgot_submit: 'Recover',
                COMPUTER: 'Computer',
                ROUTER: 'Router',
                MOBILE_DEVICE: 'Mobile device',
                SERVER: 'Server',
                OFFICE: 'Office',
                CONNECTION_ERROR: 'Can not connect to server',
                LOGIN_LENGTH: 'Bad login length',
                PASSWORD_LENGTH: 'Bad password length',
                EMAIL_FORMAT: 'Bad email format',
                LOGIN_UNIQUE: 'Login already exists',
                NAME_LENGTH: 'Bad name length',
                SURNAME_LENGTH: 'Bad surname length',
                BAD_PASSWORD: 'Bad login or password',
                BAD_MAC_ADDRESS: 'Bad MAC address format',
                HOST_NAME_LENGTH: 'Bad hostname length',
                BAD_HOST_NAME: 'Hostname contains unallowed characters',
                HOST_NAME_UNIQUE: 'Hostname already exists',
                BAD_DEVICE_TYPE: 'Bad drvice type',
                MAC_ADDRESS_UNIQUE: 'MAC address already exists',
                BAD_UUID: 'Bad item identifier',
            }
        },
        pl: {
            translations: {
                sru: 'SRU v5',
                terms: 'Regulamin SKOS',
                contact: 'Dyżury i kontakt',
                login: 'Logowanie',
                contact_email: 'Kontakt',
                duty_hours: 'Dyżury',
                sru_login: 'Zaloguj się w SRU',
                login_placeholder: 'Login lub email',
                password_placeholder: 'Hasło',
                login_submit: 'Zaloguj',
                forgot_link: 'Zapomniałeś hasła?',
                forgot_password: 'Odzyskowanie hasła',
                or: 'lub',
                forgot_submit: 'Wyślij',
                COMPUTER: 'Komputer',
                ROUTER: 'Router',
                MOBILE_DEVICE: 'Urządzenie mobilne',
                SERVER: 'Serwer',
                OFFICE: 'Biuro',
                CONNECTION_ERROR: 'Błąd połączenia z serwerem',
                LOGIN_LENGTH: 'Zła długość loginu',
                PASSWORD_LENGTH: 'Zła długość hasła',
                EMAIL_FORMAT: 'Zły format maila',
                LOGIN_UNIQUE: 'Podany login już istnieje',
                NAME_LENGTH: 'Zła długość imienia',
                SURNAME_LENGTH: 'Zła długość nazwiska',
                BAD_PASSWORD: 'Zły login lub hasło',
                BAD_MAC_ADDRESS: 'Podany MAC nie jest poprawny',
                HOST_NAME_LENGTH: 'Zła długość nazwy urządzenia',
                BAD_HOST_NAME: 'Niedozwolone znaki w nazwie urządzenia',
                HOST_NAME_UNIQUE: 'Podana nazwa urządzenie już istnieje',
                BAD_DEVICE_TYPE: 'Zły typ urządzenia',
                MAC_ADDRESS_UNIQUE: 'Podany adres MAC już istnieje',
                BAD_UUID: 'Zły identyfikator zasobu',
            }
        },
    },
    fallbackLng: "en",
    debug: false,

    // have a common namespace used around the full app
    ns: ["translations"],
    defaultNS: "translations",

    keySeparator: false, // we use content as keys

    interpolation: {
        escapeValue: false, // not needed for react!!
        formatSeparator: ","
    },

    react: {
        wait: true
    }
});

export default i18n;