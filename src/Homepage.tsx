import React from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import styles from './Homepage.module.css';
import Contact from './contact/Contact';
import Login from './authentication/login/Login';
import {WithTranslation, withTranslation} from 'react-i18next'
import Forgot from './authentication/forgot/Forgot';
import User from './user/user/User';
import DeviceForm from './device/deviceForm/DeviceForm';
import DeviceFormRouted from "./device/deviceForm/deviceFormRouted/DeviceFormRouted";
import {ErrorContext} from './ErrorContext';
import Users from "./user/users/Users";
import UserRouted from "./user/user/userRouted/UserRouted";
import Devices from './device/devices/Devices';
import send from "./helpers/api";
import NotFound from "./notFound/NotFound";
import Logout from './authentication/logout/Logout';
import UserForm from "./user/userForm/UserForm";
import UserFormRouted from "./user/userForm/userFormRouted/UserFormRouted";
import Dormitories from "./dormitory/dormitories/Dormitories";
import DormitoryRouted from "./dormitory/dormitory/dormitoryRouted/DormitoryRouted";
import DeviceRouted from "./device/device/deviceRouted/DeviceRouted";
import DormitoryFormRouted from './dormitory/dormitoryForm/dormitoryFormRouted/DormitoryFormRouted';
import DormitoryForm from './dormitory/dormitoryForm/DormitoryForm';
import RoomRouted from "./dormitory/room/roomRouted/RoomRouted";
import RoomFormRouted from "./dormitory/roomForm/roomFormRouted/RoomFormRouted";
import DeviceIPFormRouted from "./deviceIP/deviceIPForm/deviceIPFormRouted/DeviceIPFormRouted";
import UserRoomFormRouted from './userRoom/userRoomForm/userRoomFormRouted/UserRoomFormRouted';

interface IState {
    errorMessage?: string,
    loggedIn: boolean,
    dormitoryTabAllowed: boolean,
    userTabAllowed: boolean,
    deviceTabAllowed: boolean,
}

export interface IProps extends WithTranslation {
}

class Homepage extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            loggedIn: false,
            dormitoryTabAllowed: false,
            userTabAllowed: false,
            deviceTabAllowed: false,
        };
        this.setErrorMessage = this.setErrorMessage.bind(this);
        this.loginButton = this.loginButton.bind(this);
        this.logoutButton = this.logoutButton.bind(this);
    }

    setErrorMessage(errorMessage: string) {
        const {t} = this.props;
        errorMessage = t(errorMessage);
        this.setState({errorMessage: errorMessage});
        setTimeout(() => {
            if (this.state.errorMessage === errorMessage) {
                this.setState({errorMessage: ''});
            }
        }, 5000);
        window.scrollTo(0, 0);
    }

    loginButton() {
        const {t} = this.props;
        return (
            <Link to="/login/" className={`${styles.button} ${styles.link}`}>
                {t("login")}
            </Link>
        )
    }

    logoutButton() {
        return (
            <Link to="/logout/" className={`${styles.button} ${styles.link}`}>
                Logout
            </Link>
        )
    }

    checkAuth() {
        send('GET', '/user/auth/', {}, () => {
            if (!this.state.loggedIn) {
                this.setState({
                    loggedIn: true,
                });
            }
        }, () => {
        }, false);
    }

    checkDormitoryPermission() {
        send('HEAD', '/dormitory/', {}, () => {
            this.setState({
                dormitoryTabAllowed: true,
            });
        }, () => {
        }, false);
    }


    checkUserPermission() {
        send('HEAD', '/user/search/', {}, () => {
            this.setState({
                userTabAllowed: true,
            });
        }, () => {
        }, false);
    }

    checkDevicePermission() {
        send('HEAD', '/device/search/', {}, () => {
            this.setState({
                deviceTabAllowed: true,
            });
        }, () => {
        }, false);
    }

    componentDidMount() {
        this.checkAuth();
        this.checkDormitoryPermission();
        this.checkUserPermission();
        this.checkDevicePermission();
    }

    render() {
        const {t} = this.props;
        const {errorMessage, loggedIn, dormitoryTabAllowed, userTabAllowed, deviceTabAllowed} = this.state;

        return (
            <ErrorContext.Provider value={this.setErrorMessage}>
                <React.StrictMode>
                    <Router>
                        <header className={styles.header}>
                            <Link to="/" className={styles.logo}>
                                {t("sru")}
                            </Link>
                            <div className={styles.menu}>
                                {loggedIn &&
                                <Link to="/profile/" className={styles.link}>
                                    Profil
                                </Link>
                                }
                                {userTabAllowed &&
                                <Link to="/users/" className={styles.link}>
                                    Użytkownicy
                                </Link>
                                }
                                {deviceTabAllowed &&
                                <Link to="/devices/" className={styles.link}>
                                    Urządzenia
                                </Link>
                                }
                                {dormitoryTabAllowed &&
                                <Link to="/dormitories/" className={styles.link}>
                                    Akademiki
                                </Link>
                                }
                                <a href="https://pg.edu.pl/skos/regulamin/" className={styles.link}>
                                    {t("terms")}
                                </a>
                                <Link to="/contact/" className={styles.link}>
                                    {t("contact")}
                                </Link>
                                {loggedIn && this.logoutButton()}
                                {!loggedIn && this.loginButton()}
                            </div>
                        </header>
                        <div className={styles.content}>
                            {errorMessage && <div className={styles.errorContainer}>{errorMessage}</div>}
                            <Switch>
                                <Route path="/" exact component={Login}/>
                                <Route path="/contact/" exact component={Contact}/>

                                <Route path="/login/" exact component={Login}/>
                                <Route path="/logout/" exact component={Logout}/>
                                <Route path="/forgot/" exact component={Forgot}/>

                                <Route path="/devices/" exact component={Devices}/>
                                <Route path="/device/add/" exact component={DeviceForm}/>
                                <Route path="/device/:uuid/edit/" exact component={DeviceFormRouted}/>
                                <Route path="/device/:uuid/" exact component={DeviceRouted}/>
                                <Route path="/user/:userUuid/device/:uuid/ip/add" exact component={DeviceIPFormRouted}/>
                                <Route path="/user/:userUuid/device/add/" exact component={DeviceFormRouted}/>
                                <Route path="/user/:userUuid/device/:uuid/edit/" exact component={DeviceFormRouted}/>
                                <Route path="/user/:userUuid/device/:uuid/" exact component={DeviceRouted}/>
                                <Route path="/user/:userUuid/room/lease/add/" exact component={UserRoomFormRouted}/>

                                <Route path="/users/" exact component={Users}/>
                                <Route path="/profile/" exact component={User}/>
                                <Route path="/user/add/" exact component={UserForm}/>
                                <Route path="/user/:uuid/edit/" exact component={UserFormRouted}/>
                                <Route path="/user/:uuid/" exact component={UserRouted}/>

                                <Route path="/dormitories/" exact component={Dormitories}/>
                                <Route path="/dormitory/add/" exact component={DormitoryForm}/>
                                <Route path="/dormitory/:uuid/edit/" exact component={DormitoryFormRouted}/>
                                <Route path="/dormitory/:uuid/room/add/" exact component={RoomFormRouted}/>
                                <Route path="/dormitory/:uuid/room/:roomUuid/edit/" exact component={RoomFormRouted}/>
                                <Route path="/dormitory/:dormitoryUuid/room/:roomUuid/lease/add/" exact
                                       component={UserRoomFormRouted}/>
                                <Route path="/dormitory/:uuid/room/:roomUuid/" exact component={RoomRouted}/>
                                <Route path="/dormitory/:uuid/" exact component={DormitoryRouted}/>
                                <Route component={NotFound}/>
                            </Switch>
                        </div>
                    </Router>
                </React.StrictMode>
            </ErrorContext.Provider>
        );
    }
}

export default withTranslation()(Homepage);
