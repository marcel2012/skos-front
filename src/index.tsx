import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Homepage from './Homepage';
import {I18nextProvider} from 'react-i18next';
import i18n from './assets/Translations';

ReactDOM.render(
    <React.StrictMode>
        <I18nextProvider i18n={i18n}>
            <Homepage/>
        </I18nextProvider>
    </React.StrictMode>,
    document.getElementById('root')
);
