import styles from './Dormitory.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {Link, withRouter} from "react-router-dom";
import {ErrorContext} from "../../ErrorContext";
import React from 'react';
import {DormitoryDTO, InhabitantRoomResponseDTO} from '../dormitories/Dormitories';
import {RouteComponentProps} from "react-router";
import RollingBox from "../../helpers/rollingBox/RollingBox";
import TableBox from "../../helpers/tableBox/TableBox";

export interface IProps extends RouteComponentProps, WithTranslation {
    uuid?: string,
}

export interface IState {
    rooms: InhabitantRoomResponseDTO[],
    dormitory?: DormitoryDTO,
}

class Dormitory extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            rooms: [],
        };
    }

    componentDidMount() {
        this.refreshRooms();
        this.refreshDormitory();
    }

    refreshRooms() {
        const {uuid} = this.props;
        send('GET', `/dormitory/${uuid}/room/inhabitant/`, {}, (result: InhabitantRoomResponseDTO[]) => {
            if (this.state.rooms !== result) {
                this.setState({
                    rooms: result,
                });
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    refreshDormitory() {
        const {uuid} = this.props;

        send('GET', `/dormitory/${uuid}/`, {}, (result: DormitoryDTO) => {
            if (this.state.dormitory !== result) {
                this.setState({
                    dormitory: result,
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    arrayOfLength(length: number): number[] {
        let table = [];
        for (let i = 0; i < length; i++) {
            table.push(i);
        }
        return table;
    }


    render() {
        //const {t} = this.props;
        const {dormitory, rooms} = this.state;
        const {uuid} = this.props;

        if (!dormitory || !rooms) {
            return null;
        }

        const nwdPlaces = 6;

        return (
            <div>
                <h3>{dormitory.name}</h3>
                <Link to={`/dormitory/${uuid}/edit/`} className={styles.link}>Edytuj</Link>
                {rooms && rooms.filter(room => room.places).length > 0 &&
                <RollingBox title='Pokoje mieszkalne' defaultOpened={true}>
                    <TableBox>
                        <table className={styles.table}>
                            <tbody>
                            <tr>
                                <th>Nazwa</th>
                                <th>Typ</th>
                                <th colSpan={nwdPlaces} className={styles.inhabitant}>Mieszkańcy</th>
                                <th>Szczegóły</th>
                            </tr>
                            {rooms && rooms.filter(room => room.places).map(room => (
                                <tr key={room.uuid}>
                                    <td>{room.name}</td>
                                    <td>{(room.places && `${room.places}-os`) || 'techniczne'}</td>
                                    {room.places && room.users && this.arrayOfLength(room.places).map((id: number) =>
                                        <td
                                            className={styles.inhabitant}
                                            colSpan={nwdPlaces / (room.places || 1)}>
                                            {room.users && room.users[id] && (
                                                <Link to={`/user/${room.users[id].uuid}/`}>
                                                    {room.users[id].firstName} {room.users[id].lastName}
                                                </Link>
                                            )}
                                            {(!room.users || !room.users[id]) && '..... .....'}
                                        </td>)}
                                    {!room.places && <td colSpan={nwdPlaces} className={styles.inhabitant}/>}
                                    <td>
                                        <Link to={`/dormitory/${dormitory.uuid}/room/${room.uuid}/`}>
                                            Szczegóły
                                        </Link>
                                        {room.users && room.places < room.users.length &&
                                        <p>Przeludnienie</p>
                                        }
                                    </td>
                                </tr>
                            ))
                            }
                            </tbody>
                        </table>
                    </TableBox>
                </RollingBox>
                }
                {rooms && rooms.filter(room => !room.places).length > 0 &&
                <RollingBox title='Pomieszczenia techniczne' defaultOpened={true}>
                    <TableBox>
                        <table className={styles.table}>
                            <tbody>
                            <tr>
                                <th>Nazwa</th>
                                <th>Szczegóły</th>
                            </tr>
                            {rooms && rooms.filter(room => !room.places).map(room => (
                                <tr key={room.uuid}>
                                    <td>{room.name}</td>
                                    <td>
                                        <Link to={`/dormitory/${dormitory.uuid}/room/${room.uuid}/`}>
                                            Szczegóły
                                        </Link>
                                    </td>
                                </tr>
                            ))
                            }
                            </tbody>
                        </table>
                    </TableBox>
                </RollingBox>
                }
                <Link to={`/dormitory/${uuid}/room/add/`} className={styles.link}>Dodaj nowy pokój</Link>
            </div>
        );

    }
}

export default withTranslation()(withRouter(Dormitory));
Dormitory.contextType = ErrorContext;
