import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import Dormitory from "../Dormitory";

interface Params {
    uuid?: string
}

export interface IProps extends RouteComponentProps<Params> {
}


class DormitoryRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid} = this.props.match.params;

        return <Dormitory uuid={uuid}/>;
    }
}


export default DormitoryRouted;
