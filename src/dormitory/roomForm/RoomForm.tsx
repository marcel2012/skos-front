import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './RoomForm.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {withRouter} from "react-router-dom";
import {ErrorContext} from "../../ErrorContext";
import {RouteComponentProps} from "react-router";
import {InhabitantRoomResponseDTO, RoomDTO} from "../dormitories/Dormitories";
import {roomPlaces} from '../../userRoom/userRoomForm/UserRoomForm';
import RoundedBox from "../../helpers/roundedBox/RoundedBox";

export interface IProps extends RouteComponentProps, WithTranslation {
    uuid?: string,
    roomUuid?: string,
}

export interface IState {
    room: InhabitantRoomResponseDTO,
}

class RoomForm extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            room: {
                name: '',
                places: 1,
            }
        };
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangePlaces = this.handleChangePlaces.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            room: {
                ...this.state.room,
                name: event.target.value
            }
        });
    }

    handleChangePlaces(event: ChangeEvent<HTMLInputElement>) {
        const newValue = (event.target.value && parseInt(event.target.value)) || 0;
        this.setState({
            room: {
                ...this.state.room,
                places: newValue
            }
        });
    }

    handleSubmit(event: SyntheticEvent) {
        event.preventDefault();
        const {history} = this.props;
        const {uuid} = this.props;

        send('PUT', `/dormitory/${uuid}/room/`, this.state.room, () => {
            history.goBack();
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    componentDidMount() {
        this.refreshDormitory();
    }

    refreshDormitory() {
        const {uuid, roomUuid} = this.props;
        if (uuid && roomUuid) {
            send('GET', `/dormitory/${uuid}/room/${roomUuid}/`, {}, (result: RoomDTO) => {
                this.setState({
                    room: result,
                });
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }


    render() {
        const {uuid, roomUuid} = this.props;
        const {room} = this.state;

        if (!uuid) {
            return null;
        }

        return (
            <div className={styles.form}>
                {roomUuid && <h3>Edytuj pokój</h3>}
                {!roomUuid && <h3>Dodaj pokój</h3>}
                <RoundedBox>
                    <form onSubmit={this.handleSubmit}>
                        <div>
                            <h4>Dane podstawowe</h4>
                            <input value={room.name} onChange={this.handleChangeName} placeholder="Nazwa pokoju"
                                   pattern="[A-Za-z0-9]*" minLength={3} maxLength={20} required={true}/>
                        </div>
                        <div>
                            <h4>Wybierz typ pokoju</h4><br/>
                            {
                                roomPlaces && roomPlaces.map((places, index) => (
                                    <span key={places}>
                                    <input type="radio" id={`roomPlaces-${index}`} name="userType"
                                           value={index}
                                           checked={index === room.places} onChange={this.handleChangePlaces}/>
                                    <label htmlFor={`roomPlaces-${index}`}>
                                        <img alt="liczba miejsc" src={places}/>
                                        {(index && '- osobowy') || 'pomieszczenie techniczne'}
                                    </label><br/>
                                </span>
                                ))
                            }
                        </div>
                        <br/>
                        <button className={styles.button} type="submit">Zapisz</button>
                    </form>
                </RoundedBox>
            </div>
        );
    }
}

export default withTranslation()(withRouter(RoomForm));
RoomForm.contextType = ErrorContext;
