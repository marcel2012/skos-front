import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import RoomForm from '../RoomForm';

interface Params {
    uuid?: string,
    roomUuid?: string,
}

export interface IProps extends RouteComponentProps<Params> {
}


class RoomFormRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid, roomUuid} = this.props.match.params;

        return <RoomForm uuid={uuid} roomUuid={roomUuid}/>;
    }
}


export default RoomFormRouted;
