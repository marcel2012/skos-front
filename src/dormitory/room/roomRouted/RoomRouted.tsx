import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import Room from '../Room';

interface Params {
    uuid?: string,
    roomUuid?: string,
}

export interface IProps extends RouteComponentProps<Params> {
}


class RoomRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid, roomUuid} = this.props.match.params;

        return <Room uuid={uuid} roomUuid={roomUuid}/>;
    }
}


export default RoomRouted;
