import styles from './Room.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {Link, withRouter} from "react-router-dom";
import {ErrorContext} from "../../ErrorContext";
import React from 'react';
import {DormitoryDTO, InhabitantRoomResponseDTO} from '../dormitories/Dormitories';
import {RouteComponentProps} from "react-router";

export interface IProps extends RouteComponentProps, WithTranslation {
    uuid?: string,
    roomUuid?: string,
}

export interface IState {
    room?: InhabitantRoomResponseDTO,
    dormitory?: DormitoryDTO,
}

class Room extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.refreshRoom();
        this.refreshDormitory();
    }

    refreshRoom() {
        const {uuid, roomUuid} = this.props;
        send('GET', `/dormitory/${uuid}/room/${roomUuid}/inhabitant/`, {}, (result: InhabitantRoomResponseDTO) => {
            if (this.state.room !== result) {
                this.setState({
                    room: result,
                });
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    refreshDormitory() {
        const {uuid} = this.props;
        send('GET', `/dormitory/${uuid}/`, {}, (result: DormitoryDTO) => {
            if (this.state.dormitory !== result) {
                this.setState({
                    dormitory: result,
                });
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    render() {
        const {room, dormitory} = this.state;
        const {uuid, roomUuid} = this.props;

        if (!room || !dormitory) {
            return null;
        }

        return (
            <div>
                <h3>{dormitory && dormitory.name} {room.name}</h3>
                {room.places > 0 && <p>Liczba miejsc: {room.places}</p>}
                {room.places === 0 && <p>Pomieszczenie techniczne</p>}
                <Link to={`/dormitory/${uuid}/room/${roomUuid}/edit/`} className={styles.link}>Edytuj</Link>
                <Link to={`/dormitory/${uuid}/room/${roomUuid}/lease/add/`} className={styles.link}>Zamelduj</Link>
                <h3>Mieszkańcy</h3>
                {room.users && room.users.map(user => (
                    <p>
                        <Link to={`/user/${user.uuid}/`}>{user.firstName} {user.lastName}</Link>
                    </p>
                ))}
            </div>
        );


    }
}

export default withTranslation()(withRouter(Room));
Room.contextType = ErrorContext;
