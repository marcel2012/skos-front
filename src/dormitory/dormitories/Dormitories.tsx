import React from 'react';
import styles from './Dormitories.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {Link} from "react-router-dom";
import {ErrorContext} from "../../ErrorContext";
import {ProfileDTO} from "../../user/user/User";
import TableBox from "../../helpers/tableBox/TableBox";


export interface IProps extends WithTranslation {
}

export interface DormitoryDTO {
    uuid?: string,
    name: string,
}

export interface InhabitantRoomResponseDTO extends RoomDTO {
    users?: ProfileDTO[],
}

export interface RoomDTO {
    uuid?: string,
    name: string,
    places: number,
}


export interface IState {
    dormitories: DormitoryDTO[],
}

class Dormitories extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            dormitories: [],
        };
    }

    componentDidMount() {
        this.refreshDormitories();
    }

    refreshDormitories() {
        send('GET', `/dormitory/`, {}, (result: DormitoryDTO[]) => {
            if (this.state.dormitories !== result) {
                this.setState({
                    dormitories: result,
                })
            }
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    render() {
        const {dormitories} = this.state;

        return (
            <div>
                <h3>Lista akademików</h3>
                <TableBox>
                    <table className={styles.table}>
                        <tbody>
                        <tr>
                            <th>Akademik</th>
                            <th>Akcje</th>
                        </tr>
                        {dormitories.map(dormitory => (
                            <tr key={dormitory.uuid}>
                                <td>{dormitory.name}</td>
                                <td>
                                    <Link to={`/dormitory/${dormitory.uuid}/`}>
                                        Szczegóły
                                    </Link>
                                </td>
                            </tr>
                        ))
                        }
                        </tbody>
                    </table>
                </TableBox>
                <Link to="/dormitory/add/" className={styles.link}>Utwórz nowy akademik</Link>
            </div>
        );
    }
}

export default withTranslation()(Dormitories);
Dormitories.contextType = ErrorContext;
