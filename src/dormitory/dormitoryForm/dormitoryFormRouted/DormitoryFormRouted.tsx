import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import DormitoryForm from '../DormitoryForm';

interface Params {
    uuid?: string
}

export interface IProps extends RouteComponentProps<Params> {
}


class DormitoryFormRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid} = this.props.match.params;

        return <DormitoryForm uuid={uuid}/>;
    }
}


export default DormitoryFormRouted;
