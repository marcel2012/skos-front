import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './DormitoryForm.module.css';
import {withTranslation, WithTranslation} from "react-i18next";
import send, {ErrorJSON} from "../../helpers/api";
import {withRouter} from "react-router-dom";
import {ErrorContext} from "../../ErrorContext";
import {RouteComponentProps} from "react-router";
import {DormitoryDTO} from "../dormitories/Dormitories";
import RoundedBox from "../../helpers/roundedBox/RoundedBox";

export interface IProps extends RouteComponentProps, WithTranslation {
    uuid?: string,
}

export interface IState {
    dormitory: DormitoryDTO,
}

class DormitoryForm extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            dormitory: {
                name: ''
            }
        };
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeName(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            dormitory: {
                ...this.state.dormitory,
                name: event.target.value
            }
        });
    }

    handleSubmit(event: SyntheticEvent) {
        event.preventDefault();
        const {history} = this.props;

        send('PUT', '/dormitory', this.state.dormitory, () => {
            history.goBack();
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    componentDidMount() {
        this.refreshDormitory();
    }

    refreshDormitory() {
        const {uuid} = this.props;
        if (uuid) {
            send('GET', `/dormitory/${uuid}/`, {}, (result: DormitoryDTO) => {
                this.setState({
                    dormitory: result,
                });
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }


    render() {
        const {uuid} = this.props;
        const {dormitory} = this.state;

        return (
            <div className={styles.form}>
                {uuid && <h3>Edytuj akademik</h3>}
                {!uuid && <h3>Dodaj akademik</h3>}
                <RoundedBox>
                    <form onSubmit={this.handleSubmit}>
                        <h4>Dane podstawowe</h4>
                        <input value={dormitory.name} onChange={this.handleChangeName} placeholder="Nazwa akademika"
                               pattern="[A-Za-z0-9]*" minLength={3} maxLength={20} required={true}/>
                        <br/>
                        <button className={styles.button} type="submit">Zapisz</button>
                    </form>
                </RoundedBox>
            </div>
        );
    }
}

export default withTranslation()(withRouter(DormitoryForm));
DormitoryForm.contextType = ErrorContext;
