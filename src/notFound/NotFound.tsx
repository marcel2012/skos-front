import React from 'react';
import styles from './NotFound.module.css';
import notFound from '../assets/404.jpg';

const NotFound = () => {

    return (
        <div className={styles.container}>
            <h3>Page not found</h3>
            <img src={notFound} alt="Ostrich - created by 愚木混株 Cdd20"/>
        </div>
    );
}

export default NotFound;
