import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './DeviceIPForm.module.css';
import send, {ErrorJSON} from "../../helpers/api";
import {ErrorContext} from "../../ErrorContext";
import {RouteComponentProps} from "react-router";
import ipv4 from '../../assets/ipv4.svg';
import ipv6 from '../../assets/ipv6.svg';
import Paging from "../../helpers/paging/Paging";
import RoundedBox from "../../helpers/roundedBox/RoundedBox";

const icon: { [id: string]: string; } = {
    'IPv4': ipv4,
    'IPv6': ipv6,
}

export interface IProps extends RouteComponentProps {
    uuid?: string,
    userUuid?: string,
}

export interface IState {
    ipAddresses: IPAddessResponse[],
    ipAddressesPage: number,
    ipAddressesPages: number,
    ipAddressUUID?: string,
    ipAddressSearch: string,
}

export interface IPAddessResponse {
    uuid: string,
    address: string,
    ipType: string,
}

export default class DeviceIPForm extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            ipAddresses: [],
            ipAddressesPage: 0,
            ipAddressesPages: 0,
            ipAddressSearch: '',
        };
        this.handleChangeIPAddress = this.handleChangeIPAddress.bind(this);
        this.handleChangePage = this.handleChangePage.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeIPAddress(event: ChangeEvent<HTMLInputElement>) {
        this.setState({
            ipAddressSearch: event.target.value,
            ipAddressesPage: 0,
        }, this.refreshFreeIPAddresses);
    }

    handleChangePage(page: number) {
        this.setState({
            ipAddressesPage: page,
        }, this.refreshFreeIPAddresses);
    }

    handleSubmit(event: SyntheticEvent) {
        const {uuid, userUuid} = this.props;
        event.preventDefault();
        const {history} = this.props;
        const {ipAddressUUID} = this.state;

        const url = `/user/${userUuid}/device/${uuid}/dhcp/`;
        send('POST', url, {
            ipAddressUuid: ipAddressUUID,
        }, () => {
            history.goBack();
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    componentDidMount() {
        this.refreshFreeIPAddresses();
    }

    refreshFreeIPAddresses() {
        const {uuid, userUuid} = this.props;
        const {ipAddressSearch, ipAddressesPage} = this.state;
        if (uuid && userUuid) {
            const url = `/user/${userUuid}/device/${uuid}/ip/search/`;
            send('GET', url, {
                ipAddress: ipAddressSearch,
                page: ipAddressesPage,
            }, (result: { ipAddresses: IPAddessResponse[], pages: number }) => {
                if (this.state.ipAddresses !== result.ipAddresses || this.state.ipAddressesPages !== result.pages) {
                    this.setState({
                        ipAddresses: result.ipAddresses,
                        ipAddressesPages: result.pages,
                    });
                }
            }, (error: ErrorJSON) => {
                this.context(error.error);
            });
        }
    }


    render() {
        const {ipAddresses, ipAddressSearch, ipAddressUUID, ipAddressesPage, ipAddressesPages} = this.state;
        const {uuid} = this.props;

        if (!uuid) {
            return null;
        }

        return (
            <div className={styles.form}>
                <h3>Dodaj dzierżawę IP</h3>
                <RoundedBox>
                    <form onSubmit={this.handleSubmit}>
                        <div>
                            <h4>Szukaj</h4>
                            <input value={ipAddressSearch} onChange={this.handleChangeIPAddress}
                                   placeholder="Adres IP"/>
                        </div>
                        <div>
                            <h4>Wybierz adres IP</h4><br/>
                            {
                                ipAddresses && ipAddresses.map(ipAddress => (
                                    <div key={ipAddress.uuid}>
                                        <input required={true} type="radio" id={ipAddress.uuid} name="deviceType"
                                               checked={ipAddressUUID === ipAddress.uuid}
                                               onChange={() => this.setState({ipAddressUUID: ipAddress.uuid})}/>
                                        <label htmlFor={ipAddress.uuid}>
                                            IPv
                                            <img alt={ipAddress.ipType} src={icon[ipAddress.ipType]}/>
                                            {ipAddress.address}
                                        </label><br/>
                                    </div>
                                ))
                            }
                            <Paging page={ipAddressesPage} pages={ipAddressesPages}
                                    onChange={this.handleChangePage}/>
                        </div>
                        <button className={styles.button} type="submit">Zapisz</button>
                    </form>
                </RoundedBox>
            </div>
        );
    }
}

DeviceIPForm.contextType = ErrorContext;
