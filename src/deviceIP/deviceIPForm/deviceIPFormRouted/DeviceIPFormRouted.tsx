import React from 'react';
import {RouteComponentProps} from "react-router-dom";
import DeviceIPForm from '../DeviceIPForm';

interface Params {
    uuid?: string,
    userUuid?: string,
}

export interface IProps extends RouteComponentProps<Params> {
}


class DeviceIPFormRouted extends React.Component<IProps, {}> {
    render() {
        const {uuid, userUuid} = this.props.match.params;

        return <DeviceIPForm uuid={uuid} userUuid={userUuid} {...this.props}/>;
    }
}


export default DeviceIPFormRouted;
