import React from 'react';
import styles from './TableBox.module.css';

export interface IProps {
    children: React.ReactNode
}

const TableBox = (props: IProps) => {
    const {children} = props;

    return (
        <div className={styles.tableFrame}>
            {children}
        </div>
    );
}

export default TableBox;
