export default function send(method: string, path: string, content: any, callback: Function, fallback: Function, redirectUnauth: boolean = true) {
    let options;
    path = `/api${path}`;

    if (method !== 'GET' && method !== 'HEAD') {
        options = {
            method: method,
            body: JSON.stringify(content),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        };
    } else {
        options = {
            method: method,
            headers: {
                'Accept': 'application/json',
            },
        };

        const searchParams = new URLSearchParams();
        Object.keys(content).forEach(key => searchParams.append(key, content[key]));
        path += "?" + searchParams.toString();
    }

    fetch(path, options)
        .then(result => {
            if (!result.ok) {
                throw result;
            }
            if (method === 'HEAD') {
                return null;
            }
            return result.json();
        })
        .then(result => {
            callback(result);
        })
        .catch(error => {
            if (error.status === 400) {
                error.json().then((errorJson: ErrorJSON) => fallback(errorJson));
            } else if (error.status === 401 && redirectUnauth) {
                window.location.href = '/';
            } else {
                fallback({error: 'CONNECTION_ERROR'});
            }
        })
}

export interface ErrorJSON {
    error: string;
}