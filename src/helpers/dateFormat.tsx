function trailingZeros(n: number, len: number) {
    let number: string = '' + n;
    while (number.length < len) {
        number = '0' + number;
    }
    return number;
}

export function formatDate(date: Date): string {
    return date.getFullYear() + "-" + trailingZeros(date.getMonth() + 1, 2) + "-" +
        trailingZeros(date.getDate(), 2) + " " + trailingZeros(date.getHours(), 2) + ":" +
        trailingZeros(date.getMinutes(), 2);
}

export function formatDateString(date: string): string {
    if (!date) {
        return "";
    }
    return formatDate(new Date(date.replace('[UTC]', '')));
}