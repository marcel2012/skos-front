import React from 'react';
import styles from './RoundedBox.module.css';

export interface IProps {
    children: React.ReactNode
}

const RoundedBox = (props: IProps) => {
    const {children} = props;

    return (
        <div className={styles.rollingFrame}>
            {children}
        </div>
    );
}

export default RoundedBox;
