import React from 'react';
import styles from './RollingBox.module.css';
import up from "../../assets/up.svg";
import down from "../../assets/down.svg";
import RoundedBox from "../roundedBox/RoundedBox";

export interface IProps {
    defaultOpened?: boolean,
    title: string,
}

export interface IState {
    opened: boolean,
}

export default class RollingBox extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        const {defaultOpened} = props;
        this.state = {
            opened: !!defaultOpened,
        };
    }

    render() {
        const {title, children} = this.props;
        const {opened} = this.state;

        return (
            <RoundedBox>
                <h3 onClick={() => this.setState({opened: !opened})}>
                    {title}
                    {opened && <img alt={"zwiń"} className={styles.menuIcon} src={up}/>}
                    {!opened && <img alt={"rozwiń"} className={styles.menuIcon} src={down}/>}
                </h3>
                {opened && children}
            </RoundedBox>
        );
    }
}
