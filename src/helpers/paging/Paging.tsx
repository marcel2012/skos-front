import React from 'react';
import styles from './Paging.module.css';

interface IProps {
    page: number,
    pages: number,
    onChange: Function,
}


const Paging = (props: IProps) => {
    const {page, pages, onChange} = props;

    let pagesList = [];
    for (let i = 0; i < pages; i++) {
        pagesList.push(i);
    }

    return (
        <div className={styles.container}>
            {pages > 1 && pagesList.map(p => (
                <div onClick={() => onChange(p)} className={`${styles.page} ${p === page ? styles.selected : ''}`}>
                    {p + 1}
                </div>
            ))}
        </div>
    );
}

export default Paging;