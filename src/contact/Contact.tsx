import React from 'react';
import styles from './Contact.module.css';
import {useTranslation} from "react-i18next";

const Contact = () => {
    const {t} = useTranslation();

    return (
        <div className={styles.contact}>
            <h3>{t('contact_email')}</h3>
            <a href="mailto:admin@ds.pg.gda.pl">admin@ds.pg.gda.pl</a>
            <h3>{t('duty_hours')}</h3>
            <iframe
                className={styles.contactFrame}
                src="https://dyzuryapi.ds.pg.gda.pl/"
                title="iframe"
            />
        </div>
    );
}

export default Contact;
