import React from 'react';
import send from '../../helpers/api';

export interface IProps {
}


export interface IState {
}


export default class Logout extends React.Component<IProps, IState> {

    componentDidMount() {
        send('DELETE', '/user/auth/', {}, () => {
            window.location.href = '/';
        }, () => {
            window.location.href = '/';
        }, false);
    }

    render() {
        return null;
    }
}
