import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './Login.module.css';
import {WithTranslation, withTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import send, {ErrorJSON} from '../../helpers/api';
import {ErrorContext} from "../../ErrorContext";

export interface IProps extends WithTranslation {
}


export interface IState {
    login: string,
    password: string,
}

interface LoginRequestDTO {
    login: string,
    password: string,
}


class Login extends React.Component<IProps, IState> {
    constructor(props: IProps) {

        super(props);
        this.state = {
            login: 'adminn',
            password: '123456',
        };
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeLogin(event: ChangeEvent<HTMLInputElement>) {
        this.setState({login: event.target.value});
    }

    handleChangePassword(event: ChangeEvent<HTMLInputElement>) {
        this.setState({password: event.target.value});
    }


    handleSubmit(event: SyntheticEvent) {
        event.preventDefault();

        const content: LoginRequestDTO = {
            login: this.state.login,
            password: this.state.password,
        };

        send('POST', '/user/auth/', content, (result: JSON) => {
            window.location.href = '/profile/';
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    render() {
        const {t} = this.props;

        return (
            <form className={styles.form} onSubmit={this.handleSubmit}>
                <h3>{t('sru_login')}</h3>
                <input value={this.state.login} onChange={this.handleChangeLogin} placeholder={t('login_placeholder')}
                       pattern="[A-Za-z0-9]*" minLength={6} maxLength={20} required={true}/><br/>
                <input value={this.state.password} onChange={this.handleChangePassword} type="password"
                       placeholder={t('password_placeholder')} minLength={6} maxLength={20} required={true}/><br/>
                <button type="submit">{t('login_submit')}</button>
                <br/>
                <Link to="/forgot/">{t('forgot_link')}</Link>
            </form>
        );
    }
}

export default withTranslation()(Login);
Login.contextType = ErrorContext;
