import React, {ChangeEvent, SyntheticEvent} from 'react';
import styles from './Forgot.module.css';
import {WithTranslation, withTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import send, {ErrorJSON} from "../../helpers/api";

export interface IProps extends WithTranslation {
}


export interface IState {
    login: string,
}

interface ForgotRequestDTO {
    login: string,
}


class Forgot extends React.Component<IProps, IState> {
    constructor(props: IProps) {

        super(props);
        this.state = {
            login: '',
        };
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeLogin(event: ChangeEvent<HTMLInputElement>) {
        this.setState({login: event.target.value});
    }

    handleSubmit(event: SyntheticEvent) {
        event.preventDefault();

        const {login} = this.state;

        const content: ForgotRequestDTO = {
            login: login,
        };

        send('POST', '/user/auth/recover/', content, (result: JSON) => {
            window.location.href = '/profile/';
        }, (error: ErrorJSON) => {
            this.context(error.error);
        });
    }

    render() {
        const {t} = this.props;

        return (
            <form className={styles.form} onSubmit={this.handleSubmit}>
                <h3>{t('forgot_password')}</h3>
                <input value={this.state.login} onChange={this.handleChangeLogin} placeholder={t('login_placeholder')}
                       pattern="[A-Za-z0-9]*" minLength={6} required={true}/><br/>
                <button type="submit">{t('forgot_submit')}</button>
                <br/>
                <Link to="/login/">{t('login')}</Link>
            </form>
        );
    }
}

export default withTranslation()(Forgot);
