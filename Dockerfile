FROM node:latest as node

WORKDIR /build/

COPY package.json .
RUN npm install

COPY . .
RUN npm run build


FROM nginx

WORKDIR /usr/share/nginx/html/

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=node /build/build/ .

RUN find . -name *.map -exec rm {} +
